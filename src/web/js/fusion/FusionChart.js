zk.$package('fusion');
fusion.FusionChart = zk.$extends(zul.Widget, {
    _data: ' tes update js',
    _chartInstance:'',
    _template:'Column2D',
    _id:'',
    _swfPath:'/web/swf/fusion/',
    $init: function() {
        this.$supers('$init', arguments);
    },
    bind_:function(){
        this.$supers('bind_', arguments);
        this._id =  this.$n().id;
        this._renderChart();
        fusion.FusionChart.instances.push(this);
    },
    onItemClick:function(cat,lbl,vlu){
        this.fire('onItemClick',{
            category:cat,
            label:lbl,
            value:vlu
        });
    },
    unbind_:function(){
        var imax =  fusion.FusionChart.instances.length;
        var iToRemove=false;
        for(var i=0;i<imax;i++){
            if(fusion.FusionChart.instances[i].getId()==this.getId()){
                iToRemove=i;
            }
        }
        if(iToRemove){
            fusion.FusionChart.instances.splice(i);
        }
        this.$supers("unbind_",arguments);
    },
    getId:function(){
        return this._id;
    },
    getData: function () {
        return this._data;
    },
    setData: function(value) {
        if (this._data != value) {
            this._data = value;
            console.log("setting the data, then rendering chart");
            this._renderChart();
        }
    },
    _renderChart:function(){
        if(!this.$n())return;
        var s= this.$n().id;
        var curl = zk.ajaxURI(this._swfPath+this._template+'.swf',{
            au: true
        });
        this._chartInstance=null;
        var lbr = (this._width)?this._width:"";
        var tg = (this._height)?this._height:"";
        this._chartInstance = new FusionCharts(curl,this.uuid+"_chart",lbr,tg,"0","1");
        if(this._data){
            this._chartInstance.setDataXML(zUtl.encodeXML(this._data));
            this._chartInstance.render(s);
        }
    },
    getTemplate: function () {
        return this._data;
    },
    setTemplate: function(value) {
        if (this._template != value) {
            this._template = value;
            console.log("setting the template, then rendering chart");
            this._renderChart();
        }
    }
},{
    proxyJs:function(id,category,label,value){
        //alert('id:'+id+',value:'+value+',label:'+label);
        var a = fusion.FusionChart.getInstance(id);
        if(a){
            a.onItemClick(category,label,value);
        }
    },
    instances:[],
    getInstance:function(name){
        var imax = fusion.FusionChart.instances.length
        for(var i=0;i<imax;i++){
            var ins = fusion.FusionChart.instances[i];
            if(ins.getId().toLowerCase()==name.toLowerCase()){
                return ins;
            }
        }
    }
});

