/* Version.java

{{IS_NOTE
	Purpose:
		
	Description:
		
	History:
		Wed Sep 19 12:31:07     2007, Created by tomyeh
}}IS_NOTE

Copyright (C) 2007 Potix Corporation. All Rights Reserved.

{{IS_RIGHT
	This program is distributed under GPL Version 2.0 in the hope that
	it will be useful, but WITHOUT ANY WARRANTY.
}}IS_RIGHT
*/
package com.djbc.zkcomponent.fusion;

/**
 * The version of the components in this JAR file.
 * {@link #UID} must be the same as the version specified in lang-addon.xml
 *
 * @author tomyeh
 */
public class Version {
	/** Returns the version UID.
	 */
	public static final String UID = "1.0.0";
}
