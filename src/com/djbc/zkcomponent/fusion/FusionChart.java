/* FusionChart.java

 {{IS_NOTE
 Purpose:

 Description:

 History:
 Thu Feb 21 14:14:52     2008, Created by tomyeh
 }}IS_NOTE

 Copyright (C) 2008 Potix Corporation. All Rights Reserved.

 {{IS_RIGHT
 This program is distributed under GPL Version 2.0 in the hope that
 it will be useful, but WITHOUT ANY WARRANTY.
 }}IS_RIGHT
 */
package com.djbc.zkcomponent.fusion;

import com.djbc.utilities.StringUtil;
import com.djbc.zkcomponent.fusion.model.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.sys.ContentRenderer;
import org.zkoss.zul.impl.XulElement;

/**
 * @author tomyeh
 */
public class FusionChart extends XulElement {

    public static String ID_PLACEHOLDER = "zkFusionCompId";
    public static String JS_PROXIER = "fusion.FusionChart.proxyJs";
    private String data;
    private String dateFormat;
    private String ganttLineAlpha;
    private String ganttWidthPercent;
    private String gridBorderColor;
    private String gridBorderAlpha;
    private String hoverCapBorderColor;
    private String hoverCapBgColor;
    private String caption = "Grafik";
    private String numberPrefix;
    private String thousand;
    private String decimal;
    private String subCaption = "";
    private String trendValue;
    private String trendName = "trend";
    private String xAxisName;
    private String yAxisName;
    private String PYAxisName;
    private String SYAxisName;
    private ChartTemplate chartTemplate;
    private boolean showValue;
    private String template;
    private String formatNumberScale;
    private String chartRightMargin;
    private String numberScaleValue;
    private String numberScaleUnit;
    private String decimalSeparator;
    private String thousandSeparator;
    private String baseFontSize;
    private String labelDisplay;
    private String numDivLines;
    private boolean showAlternateHGridColor;
    private String palette;
    private boolean showSum;
    private boolean shownames;
    private String alternateHGridColor;
    private String alternateHGridAlpha;
    private String divLineColor;
    private String divLineAlpha;
    private String canvasBorderColor;
    private String baseFontColor;
    private String lineColor;
    private String numberSuffix;
    private String yAxisMinValue;
    private String yAxisMaxValue;
    private String adjustDiv;
    private String xAxisValueDecimals;
    private String yAxisValueDecimals;
    private String decimals;
    private String bgColor;
    private String bgAlpha;
    private String bgRatio;
    private String bgAngle;
    private String enableSmartLabels;
    private String enableRotation;
    private boolean showBorder;
    private String startingAngle;
    private boolean showLegend;
    private String bgSWF;
    private String canvasBgAlpha;
    private String canvasBorderThickness;
    private boolean showLabels;

    public void setModel(SingleSeriesModel chartModel) {
        SingleSeriesRenderer ssr = new SingleSeriesRenderer();
        String sdata = ssr.getStringXMLData(chartModel);
        this.setData(sdata);
    }

    public void setModel(CategoryModel chartModel) {
        CategoryRenderer cr = new CategoryRenderer();
        String sdata = cr.getStringXMLData(chartModel);
        this.setData(sdata);
    }

    public void setModel(MSDYModel chartModel) {
        if (this.getChartTemplate().equals(ChartTemplate.MSStackedColumn2DLineDY)) {
            System.out.println("THIS IS STACKED COLUMN DY");
            MS2DDYRenderer dr = new MS2DDYRenderer();
            String sdata = dr.getStringXMLData(chartModel, new HashMap());
            this.setData(sdata);
        } else {
            System.out.println("THIS IS NOT STACKED COLUMN DY");
            MSDYRenderer dr = new MSDYRenderer();
            String sdata = dr.getStringXMLData(chartModel, chartModel.getSeriesParam());
            this.setData(sdata);
        }

    }

    public void setModel(GanttMonthlyModel chartModel) {
        GanttRenderer dr = new GanttRenderer();
        String sdata = dr.getStringXMLData(chartModel, new HashMap(), this);
        this.setData(sdata);
    }

    @Override
    protected void renderProperties(ContentRenderer renderer) throws IOException {
        ChartColor warna = ChartColor.Black;
        super.renderProperties(renderer);
        renderer.render("data", getData());
        renderer.render("dateFormat", getDateFormat());
        renderer.render("hoverCapBorderColor", getHoverCapBorderColor());
        renderer.render("hoverCapBgColor", getHoverCapBgColor());
        renderer.render("ganttWidthPercent", getGanttWidthPercent());
        renderer.render("ganttLineAlpha", getGanttLineAlpha());
        renderer.render("gridBorderColor", getDateFormat());
        renderer.render("gridBorderAlpha", getDateFormat());
        renderer.render("template", getChartTemplate().getName());
        renderer.render("width", getWidth());
        renderer.render("height", getHeight());
        renderer.render("divLineColor", warna.parse(getDivLineColor()));
        renderer.render("lineColor", warna.parse(getLineColor()));
        renderer.render("baseFontColor", warna.parse(getBaseFontColor()));
        renderer.render("divLineAlpha", warna.parse(getDivLineAlpha()));

        renderer.render("trendValue", getTrendValue());
        renderer.render("trendName", getTrendName());
        renderer.render("xAxisName", getxAxisName());
        renderer.render("yAxisName", getyAxisName());
        renderer.render("showValues", getShowValue());
        renderer.render("formatNumberScale", getFormatNumberScale());
        renderer.render("chartRightMargin", getChartRightMargin());
        renderer.render("numberScaleValue", getNumberScaleValue());
        renderer.render("numberScaleUnit", getNumberScaleUnit());
        renderer.render("decimalSeparator", getDecimalSeparator());
        renderer.render("thousandSeparator", getThousandSeparator());
        renderer.render("baseFontSize", getBaseFontSize());
        renderer.render("labelDisplay", getLabelDisplay());
        renderer.render("numDivLines", getNumDivLines());
        renderer.render("showAlternateHGridColor", getShowAlternateHGridColor());
        renderer.render("palette", getPalette());
        renderer.render("showSum", getShowSum());
        renderer.render("shownames", getShownames());
        renderer.render("alternateHGridColor", warna.parse(getAlternateHGridColor()));
        renderer.render("alternateHGridAlpha", warna.parse(getAlternateHGridAlpha()));
        renderer.render("canvasBorderColor", warna.parse(getCanvasBorderColor()));
        renderer.render("numberSuffix", getNumberSuffix());
        renderer.render("yAxisMinValue", getyAxisMinValue());
        renderer.render("yAxisMaxValue", getyAxisMaxValue());
        renderer.render("PYAxisName", getPYAxisName());
        renderer.render("SYAxisName", getSYAxisName());
        renderer.render("adjustDiv", getAdjustDiv());
        renderer.render("xAxisValueDecimals", getxAxisValueDecimals());
        renderer.render("yAxisValueDecimals", getyAxisValueDecimals());
        renderer.render("decimals", getDecimals());
        renderer.render("bgColor", warna.parse(getBgColor()));
        renderer.render("bgAlpha", getBgAlpha());
        renderer.render("bgRatio", getBgRatio());
        renderer.render("bgAngle", getBgAngle());
        renderer.render("enableSmartLabels", getEnableSmartLabels());
        renderer.render("enableRotation", getEnableRotation());
        renderer.render("showBorder", getShowBorder());
        renderer.render("startingAngle", getStartingAngle());
        renderer.render("showLegend", getShowLegend());
        renderer.render("showLabels", getShowLegend());
    }

    private String renderChartTag(String chartData) {
        String trend = "";
        ChartColor warna = ChartColor.Black;
        Map p = new HashMap();
        p.put("caption", getCaption());
        p.put("subCaption", getSubCaption());
        if (!StringUtil.isNullOrEmpty(xAxisName)) {
            p.put("xAxisName", getxAxisName());//
        }
        if (!StringUtil.isNullOrEmpty(dateFormat)) {
            p.put("dateFormat", getDateFormat());//
        }
        if (!StringUtil.isNullOrEmpty(PYAxisName)) {
            p.put("PYAxisName", getPYAxisName());//
        }
        if (!StringUtil.isNullOrEmpty(SYAxisName)) {
            p.put("SYAxisName", getSYAxisName());//
        }
        if (!StringUtil.isNullOrEmpty(yAxisName)) {
            p.put("yAxisName", getyAxisName());//
        }
        if (!StringUtil.isNullOrEmpty(trendValue)) {
            Map tr = new HashMap();
            tr.put("startValue", getTrendValue());
            tr.put("displayValue", getTrendName());
            tr.put("valueOnRight", "1");
            trend = ChartUtil.noding("trendLines", ChartUtil.noding("line", tr));
        }//
        if (!StringUtil.isNullOrEmpty(thousand)) {
            p.put("inThousandSeparator", getThousand());//
        }
        if (!StringUtil.isNullOrEmpty(decimal)) {
            p.put("inDecimalSeparator", getDecimal());//
        }
        if (getShowValue()) {
            p.put("showValues", "1");//
        } else {
            p.put("showValues", "0");//
        }

        if (getShowSum()) {
            p.put("ShowSum", "1");//
        } else {
            p.put("ShowSum", "0");//
        }

        if (getShownames()) {
            p.put("Shownames", "1");//
        } else {
            p.put("Shownames", "0");//
        }
        if (!StringUtil.isNullOrEmpty(chartRightMargin)) {
            p.put("chartRightMargin", getChartRightMargin());//
        }
        if (!StringUtil.isNullOrEmpty(formatNumberScale)) {
            p.put("formatNumberScale", getFormatNumberScale());//
        }
        if (!StringUtil.isNullOrEmpty(numberScaleValue)) {
            p.put("numberScaleValue", getNumberScaleValue());//
        } else {
            p.put("numberScaleValue", "1000,1000,1000,1000");
        }

        if (!StringUtil.isNullOrEmpty(numberScaleUnit)) {
            p.put("numberScaleUnit", getNumberScaleUnit());//
        } else {
            p.put("numberScaleUnit", "R, J, M, T");
        }

        if (!StringUtil.isNullOrEmpty(decimalSeparator)) {
            p.put("decimalSeparator", getDecimalSeparator());//
        }
        if (!StringUtil.isNullOrEmpty(thousandSeparator)) {
            p.put("thousandSeparator", getThousandSeparator());//
        }
        if (!StringUtil.isNullOrEmpty(baseFontSize)) {
            p.put("baseFontSize", getBaseFontSize());//
        }
        if (!StringUtil.isNullOrEmpty(labelDisplay)) {
            p.put("labelDisplay", getLabelDisplay());//
        }
        if (!StringUtil.isNullOrEmpty(numDivLines)) {
            p.put("numDivLines", getNumDivLines());//
        }

        p.put("ShowAlternateHGridColor", getShowAlternateHGridColor() ? "1" : "0");//

        p.put("showLabels", getShowLabels() ? "1" : "0");//

        if (!StringUtil.isNullOrEmpty(palette)) {
            p.put("palette", getPalette());//
        }

        if (!StringUtil.isNullOrEmpty(alternateHGridColor)) {
            p.put("alternateHGridColor", warna.parse(getAlternateHGridColor()));//
        } else {
            p.put("alternateHGridColor", "FCB541");//
        }
        if (!StringUtil.isNullOrEmpty(alternateHGridAlpha)) {
            p.put("alternateHGridAlpha", (getAlternateHGridAlpha()));//
        } else {
            p.put("alternateHGridAlpha", "20");
        }
        if (!StringUtil.isNullOrEmpty(divLineColor)) {
            p.put("divLineColor", warna.parse(getDivLineColor()));//
        } else {
            p.put("divLineColor", "FCB541");
        }
        if (!StringUtil.isNullOrEmpty(divLineAlpha)) {
            p.put("divLineAlpha", getDivLineAlpha());//
        } else {
            p.put("divLineAlpha", "50");
        }
        if (!StringUtil.isNullOrEmpty(canvasBorderColor)) {
            p.put("canvasBorderColor", warna.parse(getCanvasBorderColor()));//
        } else {
            p.put("canvasBorderColor", "666666");
        }
        if (!StringUtil.isNullOrEmpty(baseFontColor)) {
            p.put("baseFontColor", warna.parse(getBaseFontColor()));//
        } else {
            p.put("baseFontColor", "666666");//
        }
        if (!StringUtil.isNullOrEmpty(lineColor)) {
            p.put("lineColor", warna.parse(getLineColor()));//
        } else {
            p.put("lineColor", "FCB541");
        }

        if (!StringUtil.isNullOrEmpty(numberSuffix)) {
            p.put("numberSuffix", getNumberSuffix());//
        }
        if (!StringUtil.isNullOrEmpty(yAxisMinValue)) {
            p.put("yAxisMinValue", getyAxisMinValue());//
        }
        if (!StringUtil.isNullOrEmpty(yAxisMaxValue)) {
            p.put("yAxisMaxValue", getyAxisMaxValue());//
        }
        if (!StringUtil.isNullOrEmpty(adjustDiv)) {
            p.put("adjustDiv", getAdjustDiv());//
        }
        if (!StringUtil.isNullOrEmpty(xAxisValueDecimals)) {
            p.put("xAxisValueDecimals", getxAxisValueDecimals());//
        }
        if (!StringUtil.isNullOrEmpty(yAxisValueDecimals)) {
            p.put("yAxisValueDecimals", getyAxisValueDecimals());//
        }
        if (!StringUtil.isNullOrEmpty(decimals)) {
            p.put("decimals", getDecimals());//
        }

        if (!StringUtil.isNullOrEmpty(bgColor)) {
            p.put("bgColor", warna.parse(getBgColor()));//
        } else {
            p.put("bgColor", "99CCFF,FFFFFF");
        }
        if (!StringUtil.isNullOrEmpty(bgAlpha)) {
            p.put("bgAlpha", (getBgAlpha()));//
        } else {
            p.put("bgAlpha", "40,100");
        }
        if (!StringUtil.isNullOrEmpty(bgRatio)) {
            p.put("bgRatio", (getBgRatio()));//
        } else {
            p.put("bgRatio", "0,100");
        }
        if (!StringUtil.isNullOrEmpty(bgAngle)) {
            p.put("bgAngle", (getBgAngle()));//
        } else {
            p.put("bgAngle", "360");
        }
        if (getShowBorder()) {
            p.put("showBorder", ("1"));//
        } else {
            p.put("showBorder", "0");
        }
        if (!StringUtil.isNullOrEmpty(startingAngle)) {
            p.put("startingAngle", (getStartingAngle()));//
        } else {
            p.put("startingAngle", "45");
        }
        if (!StringUtil.isNullOrEmpty(enableSmartLabels)) {
            p.put("enableSmartLabels", (getEnableSmartLabels()));//
        } else {
            p.put("enableSmartLabels", "1");
        }
        if (!StringUtil.isNullOrEmpty(enableRotation)) {
            p.put("enableRotation", (getEnableRotation()));//
        } else {
            p.put("enableRotation", "1");
        }
        if (getShowLegend()) {
            p.put("showLegend", "1");//
        } else {
            p.put("showLegend", "0");
        }
        if (!StringUtil.isNullOrEmpty(bgSWF)) {
            p.put("bgSWF", (getBgSWF()));//
        }
        if (!StringUtil.isNullOrEmpty(canvasBgAlpha)) {
            p.put("canvasBgAlpha", (getCanvasBgAlpha()));//
        } else {
            p.put("canvasBgAlpha", "30");
        }
        if (!StringUtil.isNullOrEmpty(canvasBorderThickness)) {
            p.put("canvasBorderThickness", (getCanvasBorderThickness()));//
        } else {
            p.put("canvasBorderThickness", "0");
        }

        return ChartUtil.noding("chart", p, chartData + trend);
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getDecimal() {
        return decimal;
    }

    public void setDecimal(String decimal) {
        this.decimal = decimal;
    }

    public String getNumberPrefix() {
        return numberPrefix;
    }

    public void setNumberPrefix(String numberPrefix) {
        this.numberPrefix = numberPrefix;
    }

    public boolean isShowValue() {
        return getShowValue();
    }

    public void setShowValue(boolean showValue) {
        this.setShowValue((Boolean) showValue);
    }

    public String getSubCaption() {
        return subCaption;
    }

    public void setSubCaption(String subCaption) {
        this.subCaption = subCaption;
    }

    public String getThousand() {
        return thousand;
    }

    public void setThousand(String thousand) {
        this.thousand = thousand;
    }

    public String getTrendName() {
        return trendName;
    }

    public void setTrendName(String trendName) {
        this.trendName = trendName;
    }

    public String getTrendValue() {
        return trendValue;
    }

    public void setTrendValue(String trendValue) {
        this.trendValue = trendValue;
    }

    public String getXAxisName() {
        return getxAxisName();
    }

    public void setXAxisName(String xAxisName) {
        this.setxAxisName(xAxisName);
    }

    public String getYAxisName() {
        return getyAxisName();
    }

    public void setYAxisName(String yAxisName) {
        this.setyAxisName(yAxisName);
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public void setData(String data) {
        data = data.replaceAll(ID_PLACEHOLDER, getUuid());
        this.data = renderChartTag(data);
        System.out.println("isi data : " + this.data);
        smartUpdate("data", this.data);
    }

    public String getData() {
        return data;
    }

    /**
     * merender attribut pada <chart atrributes>.... data ....</chart>
     *
     * @return
     */
    public ChartTemplate getChartTemplate() {
        return chartTemplate;
    }

    public void setChartTemplate(ChartTemplate chartTemplate) {
        //chartTemplate = chartTemplate.replaceAll(ID_PLACEHOLDER, getUuid());
        //String tpl= "template = '" + renderChartTag(chartTemplate.getName()) + "'";
        //System.out.println(this.data);
        //smartUpdate("template", tpl);
        this.chartTemplate = chartTemplate;
        smartUpdate("template", this.chartTemplate.getName());
    }

    public String getTemplate() {
        return getChartTemplate().getName() + ".swf?ChartNoDataText=Tidak ada data";
    }

    public void setTemplate(String template) {
        if (template.indexOf(".swf") == -1) {
            template = template.replaceAll(".swf", "");
        }
        this.template = template;
        this.setChartTemplate(ChartTemplate.parse(template));
    }

    public String getxAxisName() {
        return xAxisName;
    }

    public void setxAxisName(String xAxisName) {
        this.xAxisName = xAxisName;
    }

    public String getyAxisName() {
        return yAxisName;
    }

    public void setyAxisName(String yAxisName) {
        this.yAxisName = yAxisName;
    }

    public Boolean getShowValue() {
        return showValue;
    }

    public void setShowValue(Boolean showValue) {
        this.showValue = showValue;
    }

    public String getFormatNumberScale() {
        return formatNumberScale;
    }

    public void setFormatNumberScale(String formatNumberScale) {
        this.formatNumberScale = formatNumberScale;
    }

    public String getChartRightMargin() {
        return chartRightMargin;
    }

    public void setChartRightMargin(String chartRightMargin) {
        this.chartRightMargin = chartRightMargin;
    }

    public String getNumberScaleValue() {
        return numberScaleValue;
    }

    /**
     * @param numberScaleValue the numberScaleValue to set
     */
    public void setNumberScaleValue(String numberScaleValue) {
        this.numberScaleValue = numberScaleValue;
    }

    /**
     * @return the numberScaleUnit
     */
    public String getNumberScaleUnit() {
        return numberScaleUnit;
    }

    /**
     * @param numberScaleUnit the numberScaleUnit to set
     */
    public void setNumberScaleUnit(String numberScaleUnit) {
        this.numberScaleUnit = numberScaleUnit;
    }

    /**
     * @return the decimalSeparator
     */
    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    /**
     * @param decimalSeparator the decimalSeparator to set
     */
    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    /**
     * @return the thousandSeparator
     */
    public String getThousandSeparator() {
        return thousandSeparator;
    }

    /**
     * @param thousandSeparator the thousandSeparator to set
     */
    public void setThousandSeparator(String thousandSeparator) {
        this.thousandSeparator = thousandSeparator;
    }

    /**
     * @return the baseFontSize
     */
    public String getBaseFontSize() {
        return baseFontSize;
    }

    /**
     * @param baseFontSize the baseFontSize to set
     */
    public void setBaseFontSize(String baseFontSize) {
        this.baseFontSize = baseFontSize;
    }

    /**
     * @return the labelDisplay
     */
    public String getLabelDisplay() {
        return labelDisplay;
    }

    /**
     * @param labelDisplay the labelDisplay to set
     */
    public void setLabelDisplay(String labelDisplay) {
        this.labelDisplay = labelDisplay;
    }

    /**
     * @return the numDivLines
     */
    public String getNumDivLines() {
        return numDivLines;
    }

    /**
     * @param numDivLines the numDivLines to set
     */
    public void setNumDivLines(String numDivLines) {
        this.numDivLines = numDivLines;
    }

    /**
     * @return the showAlternateHGridColor
     */
    public Boolean getShowAlternateHGridColor() {
        return showAlternateHGridColor;
    }

    /**
     * @param showAlternateHGridColor the showAlternateHGridColor to set
     */
    public void setShowAlternateHGridColor(Boolean showAlternateHGridColor) {
        this.showAlternateHGridColor = showAlternateHGridColor;
    }

    /**
     * @return the palette
     */
    public String getPalette() {
        return palette;
    }

    /**
     * @param palette the palette to set
     */
    public void setPalette(String palette) {
        this.palette = palette;
    }

    /**
     * @return the showSum
     */
    public Boolean getShowSum() {
        return showSum;
    }

    /**
     * @param showSum the showSum to set
     */
    public void setShowSum(Boolean showSum) {
        this.showSum = showSum;
    }

    /**
     * @return the shownames
     */
    public Boolean getShownames() {
        return shownames;
    }

    /**
     * @param shownames the shownames to set
     */
    public void setShownames(Boolean shownames) {
        this.shownames = shownames;
    }

    /**
     * @return the alternateHGridColor
     */
    public String getAlternateHGridColor() {
        return alternateHGridColor;
    }

    /**
     * @param alternateHGridColor the alternateHGridColor to set
     */
    public void setAlternateHGridColor(String alternateHGridColor) {
        this.alternateHGridColor = alternateHGridColor;
    }

    /**
     * @return the alternateHGridAlpha
     */
    public String getAlternateHGridAlpha() {
        return alternateHGridAlpha;
    }

    /**
     * @param alternateHGridAlpha the alternateHGridAlpha to set
     */
    public void setAlternateHGridAlpha(String alternateHGridAlpha) {
        this.alternateHGridAlpha = alternateHGridAlpha;
    }

    /**
     * @return the divLineColor
     */
    public String getDivLineColor() {
        return divLineColor;
    }

    /**
     * @param divLineColor the divLineColor to set
     */
    public void setDivLineColor(String divLineColor) {
        this.divLineColor = divLineColor;
    }

    /**
     * @return the divLineAlpha
     */
    public String getDivLineAlpha() {
        return divLineAlpha;
    }

    /**
     * @param divLineAlpha the divLineAlpha to set
     */
    public void setDivLineAlpha(String divLineAlpha) {
        this.divLineAlpha = divLineAlpha;
    }

    /**
     * @return the canvasBorderColor
     */
    public String getCanvasBorderColor() {
        return canvasBorderColor;
    }

    /**
     * @param canvasBorderColor the canvasBorderColor to set
     */
    public void setCanvasBorderColor(String canvasBorderColor) {
        this.canvasBorderColor = canvasBorderColor;
    }

    /**
     * @return the baseFontColor
     */
    public String getBaseFontColor() {
        return baseFontColor;
    }

    /**
     * @param baseFontColor the baseFontColor to set
     */
    public void setBaseFontColor(String baseFontColor) {
        this.baseFontColor = baseFontColor;
    }

    /**
     * @return the lineColor
     */
    public String getLineColor() {
        return lineColor;
    }

    /**
     * @param lineColor the lineColor to set
     */
    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    /**
     * @return the numberSuffix
     */
    public String getNumberSuffix() {
        return numberSuffix;
    }

    /**
     * @param numberSuffix the numberSuffix to set
     */
    public void setNumberSuffix(String numberSuffix) {
        this.numberSuffix = numberSuffix;
    }

    /**
     * @return the yAxisMinValue
     */
    public String getyAxisMinValue() {
        return yAxisMinValue;
    }

    /**
     * @param yAxisMinValue the yAxisMinValue to set
     */
    public void setyAxisMinValue(String yAxisMinValue) {
        this.yAxisMinValue = yAxisMinValue;
    }

    /**
     * @return the yAxisMaxValue
     */
    public String getyAxisMaxValue() {
        return yAxisMaxValue;
    }

    /**
     * @param yAxisMaxValue the yAxisMaxValue to set
     */
    public void setyAxisMaxValue(String yAxisMaxValue) {
        this.yAxisMaxValue = yAxisMaxValue;
    }

    /**
     * @return the adjustDiv
     */
    public String getAdjustDiv() {
        return adjustDiv;
    }

    /**
     * @param adjustDiv the adjustDiv to set
     */
    public void setAdjustDiv(String adjustDiv) {
        this.adjustDiv = adjustDiv;
    }

    /**
     * @return the xAxisValueDecimals
     */
    public String getxAxisValueDecimals() {
        return xAxisValueDecimals;
    }

    /**
     * @param xAxisValueDecimals the xAxisValueDecimals to set
     */
    public void setxAxisValueDecimals(String xAxisValueDecimals) {
        this.xAxisValueDecimals = xAxisValueDecimals;
    }

    /**
     * @return the yAxisValueDecimals
     */
    public String getyAxisValueDecimals() {
        return yAxisValueDecimals;
    }

    /**
     * @param yAxisValueDecimals the yAxisValueDecimals to set
     */
    public void setyAxisValueDecimals(String yAxisValueDecimals) {
        this.yAxisValueDecimals = yAxisValueDecimals;
    }

    /**
     * @return the decimals
     */
    public String getDecimals() {
        return decimals;
    }

    /**
     * @param decimals the decimals to set
     */
    public void setDecimals(String decimals) {
        this.decimals = decimals;
    }

    /**
     * @return the bgColor
     */
    public String getBgColor() {
        return bgColor;
    }

    /**
     * @param bgColor the bgColor to set
     */
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    /**
     * @return the bgAlpha
     */
    public String getBgAlpha() {
        return bgAlpha;
    }

    /**
     * @param bgAlpha the bgAlpha to set
     */
    public void setBgAlpha(String bgAlpha) {
        this.bgAlpha = bgAlpha;
    }

    /**
     * @return the bgRatio
     */
    public String getBgRatio() {
        return bgRatio;
    }

    /**
     * @param bgRatio the bgRatio to set
     */
    public void setBgRatio(String bgRatio) {
        this.bgRatio = bgRatio;
    }

    /**
     * @return the bgAngle
     */
    public String getBgAngle() {
        return bgAngle;
    }

    /**
     * @param bgAngle the bgAngle to set
     */
    public void setBgAngle(String bgAngle) {
        this.bgAngle = bgAngle;
    }

    /**
     * @return the enableSmartLabels
     */
    public String getEnableSmartLabels() {
        return enableSmartLabels;
    }

    /**
     * @param enableSmartLabels the enableSmartLabels to set
     */
    public void setEnableSmartLabels(String enableSmartLabels) {
        this.enableSmartLabels = enableSmartLabels;
    }

    /**
     * @return the enableRotation
     */
    public String getEnableRotation() {
        return enableRotation;
    }

    /**
     * @param enableRotation the enableRotation to set
     */
    public void setEnableRotation(String enableRotation) {
        this.enableRotation = enableRotation;
    }

    /**
     * @return the showBorder
     */
    public boolean getShowBorder() {
        return showBorder;
    }

    /**
     * @param showBorder the showBorder to set
     */
    public void setShowBorder(boolean showBorder) {
        this.showBorder = showBorder;
    }

    /**
     * @return the startingAngle
     */
    public String getStartingAngle() {
        return startingAngle;
    }

    /**
     * @param startingAngle the startingAngle to set
     */
    public void setStartingAngle(String startingAngle) {
        this.startingAngle = startingAngle;
    }

    /**
     * @return the showLegend
     */
    public boolean getShowLegend() {
        return showLegend;
    }

    /**
     * @param showLegend the showLegend to set
     */
    public void setShowLegend(boolean showLegend) {
        this.showLegend = showLegend;
    }

    /**
     * @return the bgSWF
     */
    public String getBgSWF() {
        return bgSWF;
    }

    /**
     * @param bgSWF the bgSWF to set
     */
    public void setBgSWF(String bgSWF) {
        this.bgSWF = bgSWF;
    }

    /**
     * @return the canvasBgAlpha
     */
    public String getCanvasBgAlpha() {
        return canvasBgAlpha;
    }

    /**
     * @param canvasBgAlpha the canvasBgAlpha to set
     */
    public void setCanvasBgAlpha(String canvasBgAlpha) {
        this.canvasBgAlpha = canvasBgAlpha;
    }

    /**
     * @return the canvasBorderThickness
     */
    public String getCanvasBorderThickness() {
        return canvasBorderThickness;
    }

    /**
     * @param canvasBorderThickness the canvasBorderThickness to set
     */
    public void setCanvasBorderThickness(String canvasBorderThickness) {
        this.canvasBorderThickness = canvasBorderThickness;
    }

    /**
     * @return the showLabels
     */
    public boolean getShowLabels() {
        return showLabels;
    }

    /**
     * @param showLabels the showLabels to set
     */
    public void setShowLabels(boolean showLabels) {
        this.showLabels = showLabels;
    }

    public String getPYAxisName() {
        return PYAxisName;
    }

    public void setPYAxisName(String PYAxisName) {
        this.PYAxisName = PYAxisName;
    }

    public String getSYAxisName() {
        return SYAxisName;
    }

    public void setSYAxisName(String SYAxisName) {
        this.SYAxisName = SYAxisName;
    }

    public boolean isShowAlternateHGridColor() {
        return showAlternateHGridColor;
    }

    public void setShowAlternateHGridColor(boolean showAlternateHGridColor) {
        this.showAlternateHGridColor = showAlternateHGridColor;
    }

    public boolean isShowSum() {
        return showSum;
    }

    public void setShowSum(boolean showSum) {
        this.showSum = showSum;
    }

    public boolean isShownames() {
        return shownames;
    }

    public void setShownames(boolean shownames) {
        this.shownames = shownames;
    }

    public String getGanttLineAlpha() {
        return ganttLineAlpha;
    }

    public void setGanttLineAlpha(String ganttLineAlpha) {
        this.ganttLineAlpha = ganttLineAlpha;
    }

    public String getGanttWidthPercent() {
        return ganttWidthPercent;
    }

    public void setGanttWidthPercent(String ganttWidthPercent) {
        this.ganttWidthPercent = ganttWidthPercent;
    }

    public String getGridBorderAlpha() {
        return gridBorderAlpha;
    }

    public void setGridBorderAlpha(String gridBorderAlpha) {
        this.gridBorderAlpha = gridBorderAlpha;
    }

    public String getGridBorderColor() {
        return gridBorderColor;
    }

    public void setGridBorderColor(String gridBorderColor) {
        this.gridBorderColor = gridBorderColor;
    }

    public String getHoverCapBgColor() {
        return hoverCapBgColor;
    }

    public void setHoverCapBgColor(String hoverCapBgColor) {
        this.hoverCapBgColor = hoverCapBgColor;
    }

    public String getHoverCapBorderColor() {
        return hoverCapBorderColor;
    }

    public void setHoverCapBorderColor(String hoverCapBorderColor) {
        this.hoverCapBorderColor = hoverCapBorderColor;
    }

    @Override
    public void service(AuRequest request, boolean everError) {
        String cmd = request.getCommand();
//        System.out.println("do some thing here from service item");
        if (cmd.equals("onItemClick")) {
            Map data = request.getData();
            Events.postEvent("onItemClick", this, data);
        } else {
            super.service(request, everError);
        }
    }

    static {
        String s = Events.ON_BLUR;
//        System.out.println("blur event = " + s);
        addClientEvent(FusionChart.class, "onItemClick", 0);
    }
}
