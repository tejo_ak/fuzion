/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.*;

/**
 * @author User
 */
public class GanttMonthlyModel implements ChartModel {

    private final static double AVERAGE_MILLIS_PER_MONTH = 365.24 * 24 * 60 * 60 * 1000 / 12;
    private Date start;
    private Date end;
    private Integer months;
    private List<GanttChartData> datas = new ArrayList<GanttChartData>();
    private String processTitle;
    private String[] seriesColor;
    private String[] seriesTitle;
    private Integer[] pads;
    private Integer gap;


    public double calculateMonthsBetween(Date d1, Date d2) {

        double jml = (d2.getTime() - d1.getTime()) / AVERAGE_MILLIS_PER_MONTH;
        Double dmonths = new Double(Math.ceil(jml));
        months = dmonths.intValue();
        return jml;
    }

    public void calculateStartEnd() {
        for (Iterator<GanttChartData> it = datas.iterator(); it.hasNext(); ) {
            GanttChartData data = it.next();
            if (start == null) {
                start = data.getStart();
            } else if (start.after(data.getStart())) {
                start = data.getStart();
            }
            if (end == null) {
                end = data.getEnd();
            } else if (end.before(data.getEnd())) {
                end = data.getEnd();
            }
            Calendar startMonth = Calendar.getInstance();
            startMonth.setTime(start);
            startMonth.set(Calendar.DATE, 1);
            start = startMonth.getTime();

            Calendar endMonth = Calendar.getInstance();
            endMonth.setTime(end);
            endMonth.set(Calendar.DATE, endMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
            end = endMonth.getTime();
        }
    }

    public void calculateData() {
        calculateStartEnd();
        calculateMonthsBetween(start, end);
    }

    public void add(String proses, String kategori, Date start, Date end) {
        GanttChartData data = new GanttChartData(proses, kategori, start, end);
        datas.add(data);
    }

    public void add(String proses, String kategori, String hoverText, Date start, Date end) {
        GanttChartData data = new GanttChartData(proses, kategori, hoverText, start, end);
        datas.add(data);
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getMonths() {
        return months;
    }

    public void setMonths(Integer months) {
        this.months = months;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public List<GanttChartData> getDatas() {
        return datas;
    }

    public String getProcessTitle() {
        return processTitle;
    }

    public void setProcessTitle(String processTitle) {
        this.processTitle = processTitle;
    }

    public String[] getSeriesColor() {
        return seriesColor;
    }

    public void setSeriesColor(String... seriesColor) {
        this.seriesColor = seriesColor;
    }

    public String[] getSeriesTitle() {
        return seriesTitle;
    }


    public void setSeriesTitle(String... seriesTitle) {
        this.seriesTitle = seriesTitle;
    }

    public Integer[] getPads() {
        return pads;
    }

    /**
     * menentukan element toppadding pada masing masin bar gantt
     * @param pads
     */
    public void setPads(Integer... pads) {
        this.pads = pads;
    }

    public Integer getGap() {
        return gap;
    }

    /**
     * Menentukan lebar antara bar gantt pertama dengan bar gantt selanjutnya.
     * @param gap
     */
    public void setGap(Integer gap) {
        this.gap = gap;
    }
}
