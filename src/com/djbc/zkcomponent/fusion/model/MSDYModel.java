/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author User
 */
public class MSDYModel extends CategoryModel {

    private Map<String, String> seriesParam = new HashMap<String, String>();
    boolean datasetAgain = false;
    String[] secondaryAxisSeries;
    String[] primaryAxisSeries;

    public void setSecondaryAxisSeries(String... seri) {
        secondaryAxisSeries = seri;
        for (int i = 0; i < seri.length; i++) {
            String serri = seri[i];
            seriesParam.put(String.format("%s:parentYAxis", serri), "S");

        }
    }

    public void setPrimaryAxisSeries(String... seri) {
        primaryAxisSeries = seri;
        for (int i = 0; i < seri.length; i++) {
            String serri = seri[i];
            seriesParam.put(String.format("%s:parentYAxis", serri), "P");
        }
    }

    public String[] getSecondaryAxisSeries() {
        return secondaryAxisSeries;
    }

    public String[] getPrimaryAxisSeries() {
        return primaryAxisSeries;
    }

    /**
     * @param seri
     * @param attrPairs pair key=value ,... better don't use any petik ' untuk
     *                  pair ini
     */
    public void setSeriesAttributes(String seri, String... attrPairs) {
        for (int i = 0; i < attrPairs.length; i++) {
            String attr = attrPairs[i];
            if (attr != null && attr.contains("=")) {
                String[] attrs = attr.split("=");
                if (attrs.length > 0) {
                    seriesParam.put(String.format("%s:%s", seri, attrs[0]), attrs[1].replace("'", ""));
                }
            }

        }
    }


    public Map<String, String> getSeriesParam() {
        return seriesParam;
    }

    public boolean isDatasetAgain() {
        return datasetAgain;
    }

    public void setDatasetAgain(boolean datasetAgain) {
        this.datasetAgain = datasetAgain;
    }
}
