/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import com.djbc.utilities.StringUtil;
import com.djbc.zkcomponent.fusion.FusionChart;
import com.djbc.zkcomponent.fusion.category.CategoryChartData;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author jote
 */
public class ChartUtil {

    public static void main(String[] args) {
        //for testing 
        MSDYModel cm = new MSDYModel();
        cm.add("2011", "jkt", "3");
        cm.add("2011", "smg", "3");
        cm.add("2011", "blw", "3");
        cm.add("2012", "jkt", "3");
        cm.add("2012", "smg", "3");
        cm.add("2012", "blw", "3");
        CombiRenderer cr = new CombiRenderer();
        cm.setPrimaryAxisSeries("2012");
        cm.setSecondaryAxisSeries("2011");
        cm.setSeriesAttributes("2011", "showValues=1");
        String s = cr.getStringXMLData(cm, cm.getSeriesParam());

        CategoryModel cmm = new CategoryModel();


        System.out.println("s==" + s);
    }

    public static String noding(String tag, Map attributes, String childs) {
        Iterator it = attributes.entrySet().iterator();
        String attrs = "";
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            attrs += StringUtil.format("{0}='{1}' ", pairs.getKey(), pairs.getValue());
        }
        String end = (StringUtil.isNullOrEmpty(childs)) ? "/>" : StringUtil.format(">{1}</{0}>", tag, childs);
        return StringUtil.format("<{0} {1} {2}", tag, attrs, end);
    }

    public static String noding(String tag, String childs) {
        return noding(tag, new HashMap(), childs);
    }

    public static String noding(String tag, Map attributes) {
        return noding(tag, attributes, "");
    }

    public static String noding(ChartData cd) {
        Map cdata = new HashMap();
        cdata.put("value", cd.getValue());
        cdata.put("label", cd.getLabel());
        if (!StringUtil.isNullOrEmpty(cd.getTooltext())) {
            cdata.put("tooltext", cd.getTooltext());
        }
        if (!StringUtil.isNullOrEmpty(cd.getLink())) {
            cdata.put("link", cd.getLink());
        } else {
            //create default link here
            cdata.put("link", StringUtil.format("javascript:{0}(\"{1}\",\"base\",\"{2}\",\"{3}\")",//
                    FusionChart.JS_PROXIER,//
                    FusionChart.ID_PLACEHOLDER, cd.getLabel(), cd.getValue()));
        }
        if (!StringUtil.isNullOrEmpty(cd.getColor())) {
            cdata.put("color", cd.getColor());
        }
        return noding("set", cdata);
    }

    public static String noding(CategoryChartData cd) {
        if (cd == null) {
            return "";
        }
        Map cdata = new HashMap();
        cdata.put("link", StringUtil.format("javascript:{0}(\"{1}\",\"{4}\",\"{2}\",\"{3}\")",//
                FusionChart.JS_PROXIER,//
                FusionChart.ID_PLACEHOLDER,
                cd.getSeri() != null
                ? cd.getSeri().getLabel() : "",
                cd.getValue(),
                cd.getCategory() != null ? cd.getCategory().getLabel() : ""));
        cdata.put("value", cd.getValue());
        return noding("set", cdata);
    }
}
//   public static String noding(ChartData cd) {
//      Map cdata = new HashMap();
//      cdata.put("value", cd.getValue());
//      cdata.put("label", cd.getLabel());
//      if (!StringUtil.isNullOrEmpty(cd.getTooltext())) {
//         cdata.put("tooltext", cd.getTooltext());
//      }
//      if (!StringUtil.isNullOrEmpty(cd.getLink())) {
//         cdata.put("link", cd.getLink());
//      }
//
//      if (!StringUtil.isNullOrEmpty(cd.getColor())) {
//         cdata.put("color", cd.getColor());
//      }
//      return noding("set", cdata);
//   }

