/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import com.djbc.zkcomponent.fusion.category.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author jote
 */
public class MS2DDYRenderer extends CategoryRenderer {


    public String getStringXMLData(MSDYModel cm, Map renderAs) {
        Categories cats = cm.getCats();
        Series sers = cm.getSers();
        List<CategoryChartData> cds = cm.getDatas();
        String scat = "";
        for (Category ca : cats) {
            Map m = new HashMap();
            m.put("label", ca.getLabel());
            scat += ChartUtil.noding("category", m);
        }
        String scats = ChartUtil.noding("categories", scat);
        String colSSers = "";
        String lineSets = "";
        for (Seri se : sers) {

            String sser = "";
            for (Category ca : cats) {
                CategoryChartData cd = renderItem(se.getLabel(), ca.getLabel(), se.getColor(), cds);
                sser += ChartUtil.noding(cd);
            }
            Map ms = new HashMap();
            ms.put("seriesName", se.getLabel());
//            ms.put("showValues", "0");
            //perikda series khusus yang akan dirender khusus juga
            Iterator it = renderAs.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                String keypar = pairs.getKey().toString();
                if (keypar.contains(":")) {
                    String[] s = keypar.split(":");
                    if (s.length > 1) {
                        if (s[0].equalsIgnoreCase(se.getLabel())) {
                            String valpar = pairs.getValue().toString();
                            ms.put(s[1], valpar);
                        }

                    }

                }

            }

            boolean isLineset = isLineSet(se.getLabel(), cm);

            String ssers = ChartUtil.noding(isLineset ? "lineset" : "dataset", ms, sser);

            if (cm.datasetAgain) {
                ssers = ChartUtil.noding("dataset", new HashMap(), ssers);
            }
            if (isLineset) {
                lineSets += ssers;
            } else {
                colSSers += ssers;

            }

        }
        Map ms = new HashMap();
        ms.put("caption", "contoh");
        ms.put("showname", "1");
        ms.put("showValues", "0");

        return scats + ChartUtil.noding("dataset", colSSers) + lineSets;
    }

    private boolean isLineSet(String seri, MSDYModel model) {
        String[] linesets = model.getSecondaryAxisSeries();
        if (model == null || model.getPrimaryAxisSeries() == null || seri == null) {
            return false;
        }
        for (int i = 0; i < linesets.length; i++) {
            String ls = linesets[i];
            if (ls.equals(seri)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        MSDYModel ssm = new MSDYModel();
        ssm.add("Aceh", "Satu", "650");
        ssm.add("Aceh", "Dua", "500");
        ssm.add("Aceh", "Tiga", "420");
        ssm.add("Aceh", "Empat", "325");
        ssm.add("Bandung", "Satu", "751");
        ssm.add("Bandung", "Dua", "250");
        ssm.add("Bandung", "Tiga", "682");
        ssm.add("Bandung", "Empat", "756");
        ssm.add("Padang", "Satu", "15");
        ssm.add("Padang", "Dua", "20");
        ssm.add("Padang", "Tiga", "27");
        ssm.add("Padang", "Empat", "10");
        ssm.add("Actual", "Satu", "12");
        ssm.add("Actual", "Dua", "24");
        ssm.add("Actual", "Tiga", "21");
        ssm.add("Actual", "Empat", "3");
        ssm.setPrimaryAxisSeries("Aceh", "Bandung");
        ssm.setSecondaryAxisSeries("Actual");
        MS2DDYRenderer mr = new MS2DDYRenderer();
        System.out.println(mr.getStringXMLData(ssm, new HashMap()));

    }
}
