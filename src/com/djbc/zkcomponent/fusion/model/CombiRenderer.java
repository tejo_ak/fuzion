/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import com.djbc.zkcomponent.fusion.category.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jote
 */
public class CombiRenderer extends CategoryRenderer {

    public String getStringXMLData(CategoryModel cm, Map renderAs) {
        Categories cats = cm.getCats();
        Series sers = cm.getSers();
        List<CategoryChartData> cds = cm.getDatas();
        String scat = "";
        for (Category ca : cats) {
            Map m = new HashMap();
            m.put("label", ca.getLabel());
            scat += ChartUtil.noding("category", m);
        }
        String scats = ChartUtil.noding("categories", scat);
        String colSSers = "";
        for (Seri se : sers) {

            String sser = "";
            for (Category ca : cats) {
                CategoryChartData cd = renderItem(se.getLabel(), ca.getLabel(), se.getColor(), cds);
                sser += ChartUtil.noding(cd);
            }
            Map ms = new HashMap();
            ms.put("seriesName", se.getLabel());
//            ms.put("showValues", "0");
            //perikda series khusus yang akan dirender khusus juga
            Iterator it = renderAs.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                String keypar = pairs.getKey().toString();
                if (keypar.contains(":")) {
                    String[] s = keypar.split(":");
                    if (s.length > 1) {
                        if (s[0].equalsIgnoreCase(se.getLabel())) {
                            String valpar = pairs.getValue().toString();
                            ms.put(s[1], valpar);
                        }

                    }

                }

            }
            String ssers = ChartUtil.noding("dataset", ms, sser);
            colSSers += ssers;
        }
        Map ms = new HashMap();
        ms.put("caption", "contoh");
        ms.put("shownames", "1");
        ms.put("showValues", "0");
        return scats + colSSers;
    }
}
