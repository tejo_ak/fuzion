/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.ArrayList;
import com.djbc.zkcomponent.fusion.category.Categories;
import com.djbc.zkcomponent.fusion.category.Category;
import com.djbc.zkcomponent.fusion.category.CategoryChartData;
import com.djbc.zkcomponent.fusion.category.Seri;
import com.djbc.zkcomponent.fusion.category.Series;

/**
 *
 * @author jote
 */
public class CategoryModel {

   Categories cats = new Categories();
   Series sers = new Series();
   ArrayList<CategoryChartData> datas = new ArrayList<CategoryChartData>();

   public Categories getCats() {
      return cats;
   }

   public ArrayList<CategoryChartData> getDatas() {
      return datas;
   }

   public Series getSers() {
      return sers;
   }

   public void add(String seri, String category, String value) {
      Category cat = cats.add(category);
      Seri s = sers.add(seri, "");
      CategoryChartData ccd = new CategoryChartData(cat, s, value);
      datas.add(ccd);
   }

   public void add(String seri, String category, String value, ChartColor cl, String tooltext) {
      Category cat = cats.add(category);
      Seri s = sers.add(seri, cl.getColor());
      CategoryChartData ccd = new CategoryChartData(cat, s, value, tooltext);
      datas.add(ccd);
   }

   public void add(String seri, String category, String value, String tooltext) {
      Category cat = cats.add(category);
      Seri s = sers.add(seri, "");
      CategoryChartData ccd = new CategoryChartData(cat, s, value, tooltext);
      datas.add(ccd);
   }
}
