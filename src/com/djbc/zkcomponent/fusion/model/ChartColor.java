/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jote
 */
public class ChartColor {

    private String name;
    private String color;
    private static List<ChartColor> defined = new ArrayList<ChartColor>();
    public static final ChartColor Black = new ChartColor("Black", "000000");
    public static final ChartColor DarkGreen = new ChartColor("DarkGreen", "006400");
    public static final ChartColor Green = new ChartColor("Green", "008000");
    public static final ChartColor Maroon = new ChartColor("Maroon", "800000");
    public static final ChartColor Navy = new ChartColor("Navy", "000080");
    public static final ChartColor Myrtle = new ChartColor("Myrtle", "21421E");
    public static final ChartColor Bistre = new ChartColor("Bistre", "3D2B1F");
    public static final ChartColor DarkBlue = new ChartColor("DarkBlue", "00008B");
    public static final ChartColor DarkRed = new ChartColor("DarkRed", "8B0000");
    public static final ChartColor Sapphire = new ChartColor("Sapphire", "082567");
    public static final ChartColor Sangria = new ChartColor("Sangria", "92000A");
    public static final ChartColor Burgundy = new ChartColor("Burgundy", "800020");
    public static final ChartColor MidnightBlue = new ChartColor("MidnightBlue", "191970");
    public static final ChartColor Ultramarine = new ChartColor("Ultramarine", "120A8F");
    public static final ChartColor Carmine = new ChartColor("Carmine", "960018");
    public static final ChartColor Taupe = new ChartColor("Taupe", "483C32");
    public static final ChartColor Chocolate = new ChartColor("Chocolate", "7B3F00");
    public static final ChartColor Auburn = new ChartColor("Auburn", "6D351A");
    public static final ChartColor Sepia = new ChartColor("Sepia", "704214");
    public static final ChartColor Arsenic = new ChartColor("Arsenic", "3B444B");
    public static final ChartColor DarkSlateGray = new ChartColor("DarkSlateGray", "2F4F4F");
    public static final ChartColor Indigo = new ChartColor("Indigo", "4B0082");
    public static final ChartColor MediumBlue = new ChartColor("MediumBlue", "0000CD");
    public static final ChartColor ForestGreen = new ChartColor("ForestGreen", "228B22");
    public static final ChartColor Charcoal = new ChartColor("Charcoal", "464646");
    public static final ChartColor Russet = new ChartColor("Russet", "80461B");
    public static final ChartColor SaddleBrown = new ChartColor("SaddleBrown", "8B4513");
    public static final ChartColor Carnelian = new ChartColor("Carnelian", "B31B1B");
    public static final ChartColor Liver = new ChartColor("Liver", "534B4F");
    public static final ChartColor DarkOliveGreen = new ChartColor("DarkOliveGreen", "556B2F");
    public static final ChartColor Cobalt = new ChartColor("Cobalt", "0047AB");
    public static final ChartColor Eggplant = new ChartColor("Eggplant", "614051");
    public static final ChartColor FireBrick = new ChartColor("FireBrick", "B22222");
    public static final ChartColor Bole = new ChartColor("Bole", "79443B");
    public static final ChartColor Brown = new ChartColor("Brown", "A52A2A");
    public static final ChartColor Byzantium = new ChartColor("Byzantium", "702963");
    public static final ChartColor Feldgrau = new ChartColor("Feldgrau", "4D5D53");
    public static final ChartColor Blue = new ChartColor("Blue", "0000FF");
    public static final ChartColor Lava = new ChartColor("Lava", "CF1020");
    public static final ChartColor Lime = new ChartColor("Lime", "00FF00");
    public static final ChartColor Red = new ChartColor("Red", "FF0000");
    public static final ChartColor Mahogany = new ChartColor("Mahogany", "C04000");
    public static final ChartColor Olive = new ChartColor("Olive", "808000");
    public static final ChartColor Purple = new ChartColor("Purple", "800080");
    public static final ChartColor Teal = new ChartColor("Teal", "008080");
    public static final ChartColor Rust = new ChartColor("Rust", "B7410E");
    public static final ChartColor Cordovan = new ChartColor("Cordovan", "893F45");
    public static final ChartColor DarkSlateBlue = new ChartColor("DarkSlateBlue", "483D8B");
    public static final ChartColor SeaGreen = new ChartColor("SeaGreen", "2E8B57");
    public static final ChartColor Jade = new ChartColor("Jade", "00A86B");
    public static final ChartColor DarkCyan = new ChartColor("DarkCyan", "008B8B");
    public static final ChartColor DarkMagenta = new ChartColor("DarkMagenta", "8B008B");
    public static final ChartColor Cardinal = new ChartColor("Cardinal", "C41E3A");
    public static final ChartColor OliveDrab = new ChartColor("OliveDrab", "6B8E23");
    public static final ChartColor Sienna = new ChartColor("Sienna", "A0522D");
    public static final ChartColor Cerulean = new ChartColor("Cerulean", "007BA7");
    public static final ChartColor Scarlet = new ChartColor("Scarlet", "FF2400");
    public static final ChartColor Crimson = new ChartColor("Crimson", "DC143C");
    public static final ChartColor Viridian = new ChartColor("Viridian", "40826D");
    public static final ChartColor LimeGreen = new ChartColor("LimeGreen", "32CD32");
    public static final ChartColor Denim = new ChartColor("Denim", "1560BD");
    public static final ChartColor Malachite = new ChartColor("Malachite", "0BDA51");
    public static final ChartColor DimGray = new ChartColor("DimGray", "696969");
    public static final ChartColor Harlequin = new ChartColor("Harlequin", "3FFF00");
    public static final ChartColor Alizarin = new ChartColor("Alizarin", "E32636");
    public static final ChartColor OrangeRed = new ChartColor("OrangeRed", "FF4500");
    public static final ChartColor Persimmon = new ChartColor("Persimmon", "EC5800");
    public static final ChartColor DarkGoldenRod = new ChartColor("DarkGoldenRod", "B8860B");
    public static final ChartColor Raspberry = new ChartColor("Raspberry", "E30B5C");
    public static final ChartColor Ruby = new ChartColor("Ruby", "E0115F");
    public static final ChartColor Cinnabar = new ChartColor("Cinnabar", "E34234");
    public static final ChartColor Cinnamon = new ChartColor("Cinnamon", "D2691E");
    public static final ChartColor Vermilion = new ChartColor("Vermilion", "E34234");
    public static final ChartColor Copper = new ChartColor("Copper", "B87333");
    public static final ChartColor Amaranth = new ChartColor("Amaranth", "E52B50");
    public static final ChartColor MediumSeaGreen = new ChartColor("MediumSeaGreen", "3CB371");
    public static final ChartColor MediumVioletRed = new ChartColor("MediumVioletRed", "C71585");
    public static final ChartColor Redviolet = new ChartColor("Redviolet", "C71585");
    public static final ChartColor Ochre = new ChartColor("Ochre", "CC7722");
    public static final ChartColor DarkViolet = new ChartColor("DarkViolet", "9400D3");
    public static final ChartColor Xanadu = new ChartColor("Xanadu", "738678");
    public static final ChartColor Cerise = new ChartColor("Cerise", "DE3163");
    public static final ChartColor Razzmatazz = new ChartColor("Razzmatazz", "E3256B");
    public static final ChartColor Asparagus = new ChartColor("Asparagus", "7BA05B");
    public static final ChartColor Tangerine = new ChartColor("Tangerine", "F28500");
    public static final ChartColor LawnGreen = new ChartColor("LawnGreen", "7CFC00");
    public static final ChartColor LightSeaGreen = new ChartColor("LightSeaGreen", "20B2AA");
    public static final ChartColor SteelBlue = new ChartColor("SteelBlue", "4682B4");
    public static final ChartColor Bronze = new ChartColor("Bronze", "CD7F32");
    public static final ChartColor Chartreuse = new ChartColor("Chartreuse", "7FFF00");
    public static final ChartColor Rose = new ChartColor("Rose", "FF007F");
    public static final ChartColor SpringGreen = new ChartColor("SpringGreen", "00FF7F");
    public static final ChartColor Gray = new ChartColor("Gray", "808080");
    public static final ChartColor SlateGray = new ChartColor("SlateGray", "708090");
    public static final ChartColor Chestnut = new ChartColor("Chestnut", "CD5C5C");
    public static final ChartColor IndianRed = new ChartColor("IndianRed", "CD5C5C");
    public static final ChartColor Darkorange = new ChartColor("Darkorange", "FF8C00");
    public static final ChartColor RoyalBlue = new ChartColor("RoyalBlue", "4169E1");
    public static final ChartColor Pumpkin = new ChartColor("Pumpkin", "FF7518");
    public static final ChartColor Gamboge = new ChartColor("Gamboge", "E49B0F");
    public static final ChartColor Emerald = new ChartColor("Emerald", "50C878");
    public static final ChartColor Peru = new ChartColor("Peru", "CD853F");
    public static final ChartColor SlateBlue = new ChartColor("SlateBlue", "6A5ACD");
    public static final ChartColor MediumSpringGreen = new ChartColor("MediumSpringGreen", "00FA9A");
    public static final ChartColor BlueViolet = new ChartColor("BlueViolet", "8A2BE2");
    public static final ChartColor DarkOrchid = new ChartColor("DarkOrchid", "9932CC");
    public static final ChartColor LightSlateGray = new ChartColor("LightSlateGray", "778899");
    public static final ChartColor YellowGreen = new ChartColor("YellowGreen", "9ACD32");
    public static final ChartColor Brass = new ChartColor("Brass", "B5A642");
    public static final ChartColor CadetBlue = new ChartColor("CadetBlue", "5F9EA0");
    public static final ChartColor DarkTurquoise = new ChartColor("DarkTurquoise", "00CED1");
    public static final ChartColor GoldenRod = new ChartColor("GoldenRod", "DAA520");
    public static final ChartColor Orange = new ChartColor("Orange", "FFA500");
    public static final ChartColor DeepPink = new ChartColor("DeepPink", "FF1493");
    public static final ChartColor Tomato = new ChartColor("Tomato", "FF6347");
    public static final ChartColor DodgerBlue = new ChartColor("DodgerBlue", "1E90FF");
    public static final ChartColor BlueGreen = new ChartColor("BlueGreen", "00DDDD");
    public static final ChartColor Amber = new ChartColor("Amber", "FFBF00");
    public static final ChartColor DeepSkyBlue = new ChartColor("DeepSkyBlue", "00BFFF");
    public static final ChartColor Fallow = new ChartColor("Fallow", "C19A6B");
    public static final ChartColor Olivine = new ChartColor("Olivine", "9AB973");
    public static final ChartColor Amethyst = new ChartColor("Amethyst", "9966CC");
    public static final ChartColor Turquoise = new ChartColor("Turquoise", "30D5C8");
    public static final ChartColor Coral = new ChartColor("Coral", "FF7F50");
    public static final ChartColor MediumSlateBlue = new ChartColor("MediumSlateBlue", "7B68EE");
    public static final ChartColor Gold = new ChartColor("Gold", "FFD700");
    public static final ChartColor DarkSeaGreen = new ChartColor("DarkSeaGreen", "8FBC8F");
    public static final ChartColor RosyBrown = new ChartColor("RosyBrown", "BC8F8F");
    public static final ChartColor GreenYellow = new ChartColor("GreenYellow", "ADFF2F");
    public static final ChartColor MediumPurple = new ChartColor("MediumPurple", "9370D8");
    public static final ChartColor PaleVioletRed = new ChartColor("PaleVioletRed", "D87093");
    public static final ChartColor MediumAquaMarine = new ChartColor("MediumAquaMarine", "66CDAA");
    public static final ChartColor DarkKhaki = new ChartColor("DarkKhaki", "BDB76B");
    public static final ChartColor MediumOrchid = new ChartColor("MediumOrchid", "BA55D3");
    public static final ChartColor Pear = new ChartColor("Pear", "D1E231");
    public static final ChartColor MediumTurquoise = new ChartColor("MediumTurquoise", "48D1CC");
    public static final ChartColor CornflowerBlue = new ChartColor("CornflowerBlue", "6495ED");
    public static final ChartColor Saffron = new ChartColor("Saffron", "F4C430");
    public static final ChartColor Salmon = new ChartColor("Salmon", "FA8072");
    public static final ChartColor Puce = new ChartColor("Puce", "CC8899");
    public static final ChartColor LightCoral = new ChartColor("LightCoral", "F08080");
    public static final ChartColor Ecru = new ChartColor("Ecru", "C2B280");
    public static final ChartColor Lemon = new ChartColor("Lemon", "FDE910");
    public static final ChartColor SandyBrown = new ChartColor("SandyBrown", "F4A460");
    public static final ChartColor DarkSalmon = new ChartColor("DarkSalmon", "E9967A");
    public static final ChartColor DarkGray = new ChartColor("DarkGray", "A9A9A9");
    public static final ChartColor Aqua = new ChartColor("Aqua", "00FFFF");
    public static final ChartColor Cyan = new ChartColor("Cyan", "00FFFF");
    public static final ChartColor Fuchsia = new ChartColor("Fuchsia", "FF00FF");
    public static final ChartColor Magenta = new ChartColor("Magenta", "FF00FF");
    public static final ChartColor Pinkorange = new ChartColor("Pinkorange", "FF9966");
    public static final ChartColor Yellow = new ChartColor("Yellow", "FFFF00");
    public static final ChartColor LightGreen = new ChartColor("LightGreen", "90EE90");
    public static final ChartColor Tan = new ChartColor("Tan", "D2B48C");
    public static final ChartColor LightSalmon = new ChartColor("LightSalmon", "FFA07A");
    public static final ChartColor HotPink = new ChartColor("HotPink", "FF69B4");
    public static final ChartColor BurlyWood = new ChartColor("BurlyWood", "DEB887");
    public static final ChartColor Orchid = new ChartColor("Orchid", "DA70D6");
    public static final ChartColor PaleGreen = new ChartColor("PaleGreen", "98FB98");
    public static final ChartColor Lilac = new ChartColor("Lilac", "C8A2C8");
    public static final ChartColor Mustard = new ChartColor("Mustard", "FFDB58");
    public static final ChartColor Celadon = new ChartColor("Celadon", "ACE1AF");
    public static final ChartColor Silver = new ChartColor("Silver", "C0C0C0");
    public static final ChartColor SkyBlue = new ChartColor("SkyBlue", "87CEEB");
    public static final ChartColor Corn = new ChartColor("Corn", "FBEC5D");
    public static final ChartColor Maize = new ChartColor("Maize", "FBEC5D");
    public static final ChartColor Wisteria = new ChartColor("Wisteria", "C9A0DC");
    public static final ChartColor Flax = new ChartColor("Flax", "EEDC82");
    public static final ChartColor Buff = new ChartColor("Buff", "F0DC82");
    public static final ChartColor LightSkyBlue = new ChartColor("LightSkyBlue", "87CEFA");
    public static final ChartColor Heliotrope = new ChartColor("Heliotrope", "DF73FF");
    public static final ChartColor Aquamarine = new ChartColor("Aquamarine", "7FFFD4");
    public static final ChartColor LightSteelBlue = new ChartColor("LightSteelBlue", "B0C4DE");
    public static final ChartColor Plum = new ChartColor("Plum", "DDA0DD");
    public static final ChartColor Violet = new ChartColor("Violet", "EE82EE");
    public static final ChartColor Khaki = new ChartColor("Khaki", "F0E68C");
    public static final ChartColor Peachorange = new ChartColor("Peachorange", "FFCC99");
    public static final ChartColor LightBlue = new ChartColor("LightBlue", "ADD8E6");
    public static final ChartColor Thistle = new ChartColor("Thistle", "D8BFD8");
    public static final ChartColor LightPink = new ChartColor("LightPink", "FFB6C1");
    public static final ChartColor PowderBlue = new ChartColor("PowderBlue", "B0E0E6");
    public static final ChartColor LightGrey = new ChartColor("LightGrey", "D3D3D3");
    public static final ChartColor Apricot = new ChartColor("Apricot", "FBCEB1");
    public static final ChartColor PaleGoldenRod = new ChartColor("PaleGoldenRod", "EEE8AA");
    public static final ChartColor Peachyellow = new ChartColor("Peachyellow", "FADFAD");
    public static final ChartColor Wheat = new ChartColor("Wheat", "F5DEB3");
    public static final ChartColor NavajoWhite = new ChartColor("NavajoWhite", "FFDEAD");
    public static final ChartColor Pink = new ChartColor("Pink", "FFC0CB");
    public static final ChartColor PaleTurquoise = new ChartColor("PaleTurquoise", "AFEEEE");
    public static final ChartColor Mauve = new ChartColor("Mauve", "E0B0FF");
    public static final ChartColor PeachPuff = new ChartColor("PeachPuff", "FFDAB9");
    public static final ChartColor Gainsboro = new ChartColor("Gainsboro", "DCDCDC");
    public static final ChartColor Periwinkle = new ChartColor("Periwinkle", "CCCCFF");
    public static final ChartColor Moccasin = new ChartColor("Moccasin", "FFE4B5");
    public static final ChartColor Peach = new ChartColor("Peach", "FFE5B4");
    public static final ChartColor Bisque = new ChartColor("Bisque", "FFE4C4");
    public static final ChartColor Platinum = new ChartColor("Platinum", "E5E4E2");
    public static final ChartColor Champaigne = new ChartColor("Champaigne", "F7E7CE");
    public static final ChartColor BlanchedAlmond = new ChartColor("BlanchedAlmond", "FFEBCD");
    public static final ChartColor AntiqueWhite = new ChartColor("AntiqueWhite", "FAEBD7");
    public static final ChartColor PapayaWhip = new ChartColor("PapayaWhip", "FFEFD5");
    public static final ChartColor MistyRose = new ChartColor("MistyRose", "FFE4E1");
    public static final ChartColor Beige = new ChartColor("Beige", "F5F5DC");
    public static final ChartColor Lavender = new ChartColor("Lavender", "E6E6FA");
    public static final ChartColor LemonChiffon = new ChartColor("LemonChiffon", "FFFACD");
    public static final ChartColor LightGoldenRodYellow = new ChartColor("LightGoldenRodYellow", "FAFAD2");
    public static final ChartColor Cream = new ChartColor("Cream", "FFFDD0");
    public static final ChartColor Linen = new ChartColor("Linen", "FAF0E6");
    public static final ChartColor Cornsilk = new ChartColor("Cornsilk", "FFF8DC");
    public static final ChartColor OldLace = new ChartColor("OldLace", "FDF5E6");
    public static final ChartColor LightCyan = new ChartColor("LightCyan", "E0FFFF");
    public static final ChartColor LightYellow = new ChartColor("LightYellow", "FFFFE0");
    public static final ChartColor HoneyDew = new ChartColor("HoneyDew", "F0FFF0");
    public static final ChartColor WhiteSmoke = new ChartColor("WhiteSmoke", "F5F5F5");
    public static final ChartColor Seashell = new ChartColor("Seashell", "FFF5EE");
    public static final ChartColor LavenderBlush = new ChartColor("LavenderBlush", "FFF0F5");
    public static final ChartColor AliceBlue = new ChartColor("AliceBlue", "F0F8FF");
    public static final ChartColor FloralWhite = new ChartColor("FloralWhite", "FFFAF0");
    public static final ChartColor Magnolia = new ChartColor("Magnolia", "F8F4FF");
    public static final ChartColor Azure = new ChartColor("Azure", "F0FFFF");
    public static final ChartColor Ivory = new ChartColor("Ivory", "FFFFF0");
    public static final ChartColor MintCream = new ChartColor("MintCream", "F5FFFA");
    public static final ChartColor GhostWhite = new ChartColor("GhostWhite", "F8F8FF");
    public static final ChartColor Snow = new ChartColor("Snow", "FFFAFA");
    public static final ChartColor White = new ChartColor("White", "FFFFFF");

   @SuppressWarnings("static-access")
    protected ChartColor(String name, String color) {
        this.name = name;
        this.color = color;
        this.defined.add(this);
    }

   public ChartColor() {
   }

    public String parse(String name) {
        ChartColor d = Black;
        try {
            for (ChartColor c : defined) {
                if (c.getName().equalsIgnoreCase(name)) {
                    System.out.println("Warna yg dipilih :" + c.getColor());
                    d = c;
                    return c.getColor();
                }
            }
        } catch (Exception e) {
            System.out.println("error parsing warna template:" + e.getMessage());
            return d.getColor();
        }
        return d.getColor();
    }

    public String getName() {
        return name;
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(String color) {
        this.color = color;
    }
}