/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author jote
 */
public class SingleSeriesRenderer implements ChartRenderer {

    private String xmlData;

    public String getStringXMLData(ChartModel cm) {
        if (cm instanceof SingleSeriesModel) {
            return getStringXMLData((SingleSeriesModel) cm);
        }
        throw new RuntimeException("renderer ini hanya support model SingleSeriesModel, bukan " + cm.getClass());
    }

    public String getStringXMLData(SingleSeriesModel cm) {
        String datas = "";
        List<ChartData> cds = cm.getDatas();
        for (ChartData cd : cds) {
            String data = ChartUtil.noding(cd);
            datas += data;
        }
        return datas;
    }
}
