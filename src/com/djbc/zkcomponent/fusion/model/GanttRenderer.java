/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import com.djbc.utilities.StringUtil;
import com.djbc.zkcomponent.fusion.FusionChart;

import java.text.SimpleDateFormat;
import java.util.*;

import com.sun.org.apache.bcel.internal.generic.DDIV;
import org.zkoss.zul.GanttModel;

/**
 * @author User
 */
public class GanttRenderer implements ChartRenderer {

    public String getStringXMLData(GanttMonthlyModel cm, Map renderAs, FusionChart fs) {
        cm.calculateData();
        String df = fs == null || fs.getDateFormat() == null || "".equals(fs.getDateFormat()) ? "dd-MMM-yyyy" : fs.getDateFormat();
        SimpleDateFormat sdfChart = new SimpleDateFormat(df);
        SimpleDateFormat sdfSimple = new SimpleDateFormat("dd-MM-yyyy");
        Map<String, Integer> procesId = new HashMap<String, Integer>();
        Map<String, String> taskId = new HashMap<String, String>();
        List<GanttChartData> datas = cm.getDatas();
        int id = 0;
        int taskeId = 0;
        String proses = "";
        String startColumn = "";
        String endColumn = "";
        String task = "";
        for (Iterator<GanttChartData> it = datas.iterator(); it.hasNext(); ) {
            GanttChartData data = it.next();
            if (taskId.get(data.getKategori()) == null) {
                taskeId++;
                Integer gap = cm.getGap() != null ? cm.getGap() : 15;
                Integer pad = (taskeId * gap - gap) + 5;
                taskId.put(data.getKategori() + "_padding", getPad(taskeId, pad.toString(), cm));
                String taskTitle = getTitle(taskeId, cm);
                if (taskTitle != null && !"".equals(taskTitle)) {
                    taskId.put(data.getKategori() + "_title", taskTitle);
                }
                String taskColor = getColor(taskeId, cm);
                if (taskColor != null && !"".equals(taskColor)) {
                    taskId.put(data.getKategori() + "_color", taskColor);
                }
                if (taskeId == 1) {
                    taskId.put(data.getKategori(), "");
                } else if (taskeId > 1) {
                    taskId.put(data.getKategori(), "-" + taskeId + 1);
                }
            }
            if (procesId.get(data.getProses()) == null) {
                id++;
                procesId.put(data.getProses(), id);
                Map p = new HashMap();
                p.put("name", data.getProses());
                p.put("id", id);
                proses += ChartUtil.noding("process", p, "");
                Map sp = new HashMap();
                sp.put("label", sdfChart.format(data.getStart()));
                sp.put("valign", "top");
                sp.put("align", "left");
                startColumn += ChartUtil.noding("text", sp, "");
                sp.put("label", sdfChart.format(data.getEnd()));
                endColumn += ChartUtil.noding("text", sp, "");
            }

            Map taskMap = new HashMap();
            taskMap.put("name", data.getKategori());
            taskMap.put("processId", procesId.get(data.getProses()));
            String hoverText = data.getHoverText();
            if (hoverText != null && !"".equals(hoverText)) {
                taskMap.put("hoverText", StringUtil.nvl(taskId.get(data.getKategori() + "_title"), "") + "{br}" + hoverText);
            }
            String color = StringUtil.nvl(taskId.get(data.getKategori() + "_color"), "");
            if (color != null && !"".equals(color)) {
                taskMap.put("color", color);
            }
            taskMap.put("id", procesId.get(data.getProses()) + taskId.get(data.getKategori()));
            taskMap.put("topPadding", taskId.get(data.getKategori() + "_padding"));

            taskMap.put("start", sdfSimple.format(data.getStart()));
            taskMap.put("height", "10");
            taskMap.put("end", sdfSimple.format(data.getEnd()));
//            taskMap.put("label", sdfChart.format(data.getStart()));
            task += ChartUtil.noding("task", taskMap, "");

        }
        Map<String, String> prosesMap = new HashMap<String, String>();

        prosesMap.put("headerText", cm.getProcessTitle() != null && !"".equals(cm.getProcessTitle()) ? cm.getProcessTitle() : "Activity");
        prosesMap.put("align", "left");
        prosesMap.put("valign", "top");
        String prosess = ChartUtil.noding("processes", prosesMap, proses);
        Map<String, String> dataMap = new HashMap<String, String>();
        dataMap.put("headerText", "Start");
        String dataColStart = ChartUtil.noding("dataColumn", dataMap, startColumn);
        dataMap.put("headerText", "End");
        String dataColEnd = ChartUtil.noding("dataColumn", dataMap, endColumn);
        Map<String, String> tableMap = new HashMap<String, String>();
        tableMap.put("showProcessName", "1");
        String startEndColumn = ChartUtil.noding("dataTable", dataMap, dataColStart + dataColEnd);

        Map<String, String> taskesMap = new HashMap<String, String>();
//        taskesMap.put("showProcessName", "1");
        String tasks = ChartUtil.noding("tasks", taskesMap, task);

        Map<String, String> titleMap = new HashMap<String, String>();
        titleMap.put("start", sdfSimple.format(cm.getStart()));
        titleMap.put("end", sdfSimple.format(cm.getEnd()));
        titleMap.put("name", fs != null ? fs.getCaption() : "");
        titleMap.put("isBold", "1");

        String titleCat = ChartUtil.noding("category", titleMap, "");
        String titleCatt = ChartUtil.noding("categories", new HashMap<String, String>(), titleCat);

        Map<String, String> subTitleMap = new HashMap<String, String>();
        subTitleMap.put("start", sdfSimple.format(cm.getStart()));
        subTitleMap.put("end", sdfSimple.format(cm.getEnd()));
        subTitleMap.put("isBold", "1");
        subTitleMap.put("fontSize", "16");
        subTitleMap.put("name", fs != null ? fs.getSubCaption() != null ? fs.getSubCaption() : "Months" : "Months");
        String subTitleCat = ChartUtil.noding("category", titleMap, "");
        String subTitleCatt = ChartUtil.noding("categories", new HashMap<String, String>(), titleCat);
        String[] bulans = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        Date currentDate = null;
        String catMonths = "";
        for (int i = 0; i < cm.getMonths(); i++) {
            if (currentDate == null) {
                currentDate = cm.getStart();
            } else {
                Calendar calNextMonth = Calendar.getInstance();
                calNextMonth.setTime(currentDate);
                calNextMonth.add(Calendar.MONTH, 1);
                currentDate = calNextMonth.getTime();
            }
            Calendar calStart = Calendar.getInstance();
            calStart.setTime(currentDate);
            calStart.set(Calendar.DATE, 1);
            Date startMonth = calStart.getTime();
            calStart.set(Calendar.DATE, calStart.getActualMaximum(Calendar.DAY_OF_MONTH));
            Date endMonth = calStart.getTime();
            Map<String, String> monthMap = new HashMap<String, String>();
            monthMap.put("start", sdfSimple.format(startMonth));
            monthMap.put("name", bulans[calStart.get(Calendar.MONTH)]);
            monthMap.put("end", sdfSimple.format(endMonth));
            catMonths += ChartUtil.noding("category", monthMap, "");


        }


        String catMonthh = ChartUtil.noding("categories", new HashMap(), catMonths);

        return titleCatt + subTitleCatt + catMonthh + prosess + startEndColumn + tasks;
    }

    public String getPad(Integer taskId, String defaultPad, GanttMonthlyModel model) {
        if (model.getPads() != null && model.getPads().length >= taskId && model.getPads()[taskId - 1] != null) {
            return model.getPads()[taskId - 1].toString();
        }
        return defaultPad;
    }

    public String getColor(Integer taskId, GanttMonthlyModel model) {
        if (model.getSeriesColor() != null && model.getSeriesColor().length >= taskId && model.getSeriesColor()[taskId - 1] != null) {
            return model.getSeriesColor()[taskId - 1].toString();
        }
        return null;
    }

    public String getTitle(Integer taskId, GanttMonthlyModel model) {
        if (model.getSeriesTitle() != null && model.getSeriesTitle().length >= taskId && model.getSeriesTitle()[taskId - 1] != null) {
            return model.getSeriesTitle()[taskId - 1].toString();
        }
        return null;
    }

    public String getStringXMLData(ChartModel cm) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public static void main(String[] args) {
        GanttMonthlyModel gm = new GanttMonthlyModel();
        gm.add("Aceh", "object", "Program Audit Investigasi Umum mendasar", StringUtil.parseDate("06-Jan-2013"), StringUtil.parseDate("02-Feb-2013"));
        gm.add("Topik 2", "object", StringUtil.parseDate("01-Feb-2013"), StringUtil.parseDate("02-May-2013"));
        gm.add("Topik 3", "object", StringUtil.parseDate("01-Mar-2013"), StringUtil.parseDate("06-Aug-2013"));
        gm.add("Topik 4", "object", StringUtil.parseDate("01-Jul-2013"), StringUtil.parseDate("02-Sep-2013"));

        gm.add("Aceh", "st", StringUtil.parseDate("06-Jan-2013"), StringUtil.parseDate("02-Feb-2013"));
        gm.add("Topik 2", "st", StringUtil.parseDate("01-Feb-2013"), StringUtil.parseDate("02-May-2013"));
        gm.add("Topik 3", "st", StringUtil.parseDate("01-Mar-2013"), StringUtil.parseDate("06-Aug-2013"));
        gm.add("Topik 4", "st", StringUtil.parseDate("01-Jul-2013"), StringUtil.parseDate("02-Sep-2013"));


        gm.setProcessTitle("Object Audits");
        gm.setSeriesTitle("Audit Program", "Assigment Letter");
        gm.setGap(16);
        gm.setSeriesColor("0903c3");
//        FusionChart fc = new FusionChart();
//        fc.setCaption("Judul");
//        fc.setSubCaption("Sub Judul");
//        fc.setDateFormat("dd-MMM-yyyy");
        GanttRenderer gr = new GanttRenderer();

        System.out.println(gr.getStringXMLData(gm, new HashMap(), null));
    }


}
