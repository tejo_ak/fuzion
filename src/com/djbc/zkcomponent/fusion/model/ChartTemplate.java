/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jote
 */
public class ChartTemplate {

    private String name;
    private ChartType chartType;
    private static List<ChartTemplate> defined = new ArrayList<ChartTemplate>();
    public static final ChartTemplate MSColumn3D = new ChartTemplate("MSColumn3D", ChartType.CATEGORY);
    public static final ChartTemplate MSColumn2D = new ChartTemplate("MSColumn2D", ChartType.CATEGORY);
    public static final ChartTemplate Area2D = new ChartTemplate("Area2D", ChartType.SINGLE);
    public static final ChartTemplate MSArea = new ChartTemplate("MSArea", ChartType.CATEGORY);
    public static final ChartTemplate Column2D = new ChartTemplate("Column2D", ChartType.SINGLE);
    public static final ChartTemplate Column3D = new ChartTemplate("Column3D", ChartType.SINGLE);
    public static final ChartTemplate MSColumnLine3D = new ChartTemplate("MSColumnLine3D", ChartType.COMBY);
    public static final ChartTemplate MSCombiDY2D = new ChartTemplate("MSCombiDY2D", ChartType.COMBY);
    public static final ChartTemplate MSColumn3DLineDY = new ChartTemplate("MSColumn3DLineDY", ChartType.COMBY);
    public static final ChartTemplate StackedColumn3DLineDY = new ChartTemplate("StackedColumn3DLineDY", ChartType.COMBY);
    public static final ChartTemplate MSStackedColumn2DLineDY = new ChartTemplate("MSStackedColumn2DLineDY", ChartType.COMBY);
    public static final ChartTemplate StackedColumn2D = new ChartTemplate("StackedColumn2D", ChartType.COMBY);
    public static final ChartTemplate Doughnut3D = new ChartTemplate("Doughnut3D", ChartType.SINGLE);
    public static final ChartTemplate Doughnut2D = new ChartTemplate("Doughnut2D", ChartType.SINGLE);
    public static final ChartTemplate Bar2D = new ChartTemplate("Bar2D", ChartType.SINGLE);
    public static final ChartTemplate MSBar2D = new ChartTemplate("MSBar2D", ChartType.CATEGORY);
    public static final ChartTemplate MSCombi2D = new ChartTemplate("MSCombi2D", ChartType.CATEGORY);
    public static final ChartTemplate Line = new ChartTemplate("Line", ChartType.CATEGORY);
    public static final ChartTemplate MSLine = new ChartTemplate("MSLine", ChartType.CATEGORY);
    public static final ChartTemplate Pie2D = new ChartTemplate("Pie2D", ChartType.SINGLE);
    public static final ChartTemplate Pie3D = new ChartTemplate("Pie3D", ChartType.SINGLE);
    public static final ChartTemplate Gantt = new ChartTemplate("Gantt", ChartType.SINGLE);

     protected ChartTemplate(String name, ChartType chartType) {
        this.name = name;
        this.chartType = chartType;
        this.defined.add(this);
    }

    public static ChartTemplate parse(String name) {
        if (name == null) {
            System.out.println("proses parsing tidak berhasil karena null");
            return null;
        }
        try {
            for (ChartTemplate c : defined) {
                if (c.getName().equalsIgnoreCase(name)) {
                    System.out.println("template found:" + c.getName());
                    return c;
                }
            }
        } catch (Exception e) {
            System.out.println("error parsing chart template:" + e.getMessage());
        }

        return null;
    }

    public ChartType getChartType() {
        return chartType;
    }

    public String getName() {
        return name;
    }
}
