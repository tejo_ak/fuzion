/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.djbc.zkcomponent.fusion.category.Categories;
import com.djbc.zkcomponent.fusion.category.Category;
import com.djbc.zkcomponent.fusion.category.CategoryChartData;
import com.djbc.zkcomponent.fusion.category.Seri;
import com.djbc.zkcomponent.fusion.category.Series;

/**
 *
 * @author jote
 */
public class CategoryRenderer implements ChartRenderer {

   public String getStringXMLData(ChartModel cm) {
      throw new UnsupportedOperationException("Not supported yet.");
   }

   public String getStringXMLData(CategoryModel cm) {
      Categories cats = cm.getCats();
      Series sers = cm.getSers();
      List<CategoryChartData> cds = cm.getDatas();
      String scat = "";
      for (Category ca : cats) {
         Map m = new HashMap();
         m.put("label", ca.getLabel());
         scat += ChartUtil.noding("category", m);
      }
      String scats = ChartUtil.noding("categories", scat);
      String colSSers = "";
      for (Seri se : sers) {

         String sser = "";
         for (Category ca : cats) {
            CategoryChartData cd = renderItem(se.getLabel(), ca.getLabel(), se.getColor(), cds);
            sser += ChartUtil.noding(cd);
         }
         Map ms = new HashMap();
         ms.put("seriesName", se.getLabel());
         ms.put("showValues", "0");
         ms.put("color", se.getColor());
         String ssers = ChartUtil.noding("dataset", ms, sser);
         colSSers += ssers;
      }
      Map ms = new HashMap();
      ms.put("caption", "contoh");
      ms.put("shownames", "1");
      ms.put("showValues", "0");
      return scats + colSSers;
   }

   public CategoryChartData renderItem(String seri, String cat, String color, List<CategoryChartData> cds) {
      CategoryChartData found = null;
      for (CategoryChartData cd : cds) {
         if (cd.getCategory().getLabel().equals(cat) && cd.getSeri().getLabel().equals(seri)) {
            found = cd;
            break;
         }
      }
      cds.remove(found);
      return found;
   }
}
