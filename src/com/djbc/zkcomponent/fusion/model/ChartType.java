/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jote
 */
public class ChartType {

    private String name;
    private static List<ChartType> defined = new ArrayList<ChartType>();
    public static final ChartType SINGLE = new ChartType("single");
    public static final ChartType CATEGORY = new ChartType("multi");
    public static final ChartType COMBY = new ChartType("comby");
    public static final ChartType GANTT = new ChartType("gantt");

    protected ChartType(String name) {
        this.name = name;
        defined.add(this);
    }

    public static ChartType parse(String name) {
        if (name == null) {
            return null;
        }
        for (ChartType c : defined) {
            if (c.getName().equalsIgnoreCase(name)) {
                return c;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
