/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jote
 */
public class SingleSeriesModel implements ChartModel {

    private List<ChartData> datas = new ArrayList<ChartData>();

    public static SingleSeriesModel newInstance() {
        return new SingleSeriesModel();
    }

    public SingleSeriesModel clear() {
        datas.clear();
        return this;
    }

    public SingleSeriesModel addSerie(String label, String value) {
        ChartData cd = new ChartData(label, value, "", "");
        datas.add(cd);
        return this;
    }

    public SingleSeriesModel addSerie(ChartData cd) {
        datas.add(cd);
        return this;
    }

    public List<ChartData> getDatas() {
        return datas;
    }

    ;
}
