/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.model;

import java.util.Date;

/**
 * @author User
 */
public class GanttChartData {

    private String proses;
    private String kategori;
    private String hoverText;

    private Date start;
    private Date end;

    public GanttChartData() {
    }

    public GanttChartData(String proses, String kategori, Date start, Date end) {
        this.proses = proses;
        this.kategori = kategori;
        this.start = start;
        this.end = end;
    }

    public GanttChartData(String proses, String kategori, String hoverText, Date start, Date end) {
        this.proses = proses;
        this.kategori = kategori;
        this.hoverText = hoverText;
        this.start = start;
        this.end = end;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getProses() {
        return proses;
    }

    public void setProses(String proses) {
        this.proses = proses;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public String getHoverText() {
        return hoverText;
    }

    public void setHoverText(String hoverText) {
        this.hoverText = hoverText;
    }
}
