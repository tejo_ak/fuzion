    /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.category;

import java.util.ArrayList;

/**
 *
 * @author jote
 */
public class Series extends ArrayList<Seri> {

    public Seri add(String seriName, String color) {
        //setiap kali menambahkan seri, di periksa apakah dalam daftar seri ini data yang akan ditambahkan
        //telah ada sebelumnya. apa bila telah ada, maka data tidak ditambahkan kedalam list
        for (Seri s : this) {
            if (s.getLabel().equals(seriName)) {
                return s;
            }
        }
        Seri se = new Seri(seriName, color);
        this.add(se);
        return se;
    }
}
