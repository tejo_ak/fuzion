/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.category;

/**
 *
 * @author jote
 */
public class Category {

    private String label;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Category(String label) {
        this.label = label;
    }
    
}
