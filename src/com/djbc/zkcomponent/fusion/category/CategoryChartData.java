/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.category;

import com.djbc.zkcomponent.fusion.model.ChartColor;

/**
 *
 * @author jote
 */
public class CategoryChartData {

   private Seri seri;
   private Category category;
   private String value;
   private String color;
   private String tooltext;

   public CategoryChartData(Category category, Seri seri, String value) {
      this.seri = seri;
      this.category = category;
      this.value = value;
   }

   public CategoryChartData(Category category, Seri seri, String value,String tooltext) {
      this.seri = seri;
      this.category = category;
      this.value = value;
      if (!(seri.getColor() == null)) {
         this.color = seri.getColor();
      }
       this.tooltext = tooltext;
   }

   public Category getCategory() {
      return category;
   }

   public void setCategory(Category category) {
      this.category = category;
   }

   public Seri getSeri() {
      return seri;
   }

   public void setSeri(Seri seri) {
      this.seri = seri;
   }

   public String getValue() {
      return value;
   }

   public void setValue(String value) {
      this.value = value;
   }

   /**
    * @return the color
    */
   public String getColor() {
      return color;
   }

   /**
    * @param color the color to set
    */
   public void setColor(String color) {
      this.color = color;
   }

   /**
    * @return the tooltext
    */
   public String getTooltext() {
      return tooltext;
   }

   /**
    * @param tooltext the tooltext to set
    */
   public void setTooltext(String tooltext) {
      this.tooltext = tooltext;
   }
}
