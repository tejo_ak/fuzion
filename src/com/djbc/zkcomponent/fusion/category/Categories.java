/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.category;

import java.util.ArrayList;

/**
 *
 * @author jote
 */
public class Categories extends ArrayList<Category> {

    public Category add(String categoryName) {
        //setiap kali menambahkan seri, di periksa apakah dalam daftar seri ini data yang akan ditambahkan
        //telah ada sebelumnya. apa bila telah ada, maka data tidak ditambahkan kedalam list
        for (Category c : this) {
            if (c.getLabel().equals(categoryName)) {
                return c;
            }
        }
        Category ca = new Category(categoryName);
        this.add(ca);
        return ca;
    }
}
