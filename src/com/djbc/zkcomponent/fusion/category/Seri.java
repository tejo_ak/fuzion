/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.djbc.zkcomponent.fusion.category;

/**
 *
 * @author jote
 */
public class Seri {

    public Seri(String label) {
        this.label = label;
    }

   public Seri(String label, String color) {
      this.label = label;
      this.color = color;
   }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    private String label;
    private String color;

   /**
    * @return the color
    */
   public String getColor() {
      return color;
   }

   /**
    * @param color the color to set
    */
   public void setColor(String color) {
      this.color = color;
   }
}
